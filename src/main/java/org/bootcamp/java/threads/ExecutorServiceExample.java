package org.bootcamp.java.threads;

import java.time.LocalDateTime;
import java.util.concurrent.*;

public class ExecutorServiceExample {

    public static void main(String[] args) throws Exception {
        System.out.println("Starting main method from thread " + Thread.currentThread().getName());

        simpleBackgroundTask();

        poolOfTasks();

        System.out.println("End of main method from thread " + Thread.currentThread().getName());

    }

    public static void simpleBackgroundTask() throws Exception {
        ExecutorService executor = Executors.newSingleThreadExecutor();

        executor.submit(new Runnable() {
            @Override
            public void run() {
                System.out.println("Executing task in background from thread " + Thread.currentThread().getName());
            }
        });
        executor.shutdownNow();
        executor.awaitTermination(10, TimeUnit.SECONDS);
    }

    public static void poolOfTasks() throws Exception {
        ExecutorService executor = Executors.newFixedThreadPool(3);

        Callable<LocalDateTime> callable = new Callable<LocalDateTime>() {
            @Override
            public LocalDateTime call() throws Exception {
                System.out.println("Starting sleep in thread " + Thread.currentThread().getName());
                Thread.sleep(1000 * 5);
                System.out.println("Ending sleep in thread " + Thread.currentThread().getName() + " returning " + LocalDateTime.now());
                return LocalDateTime.now();
            }
        };


        for(int i=0; i < 10; i++){
            Future<LocalDateTime> result = executor.submit(callable);
        }
    }

}
