package org.bootcamp.java.threads;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

public class TimerExample {

    public static void main(String[] args){
        System.out.println("Starting main method from thread " + Thread.currentThread().getName());
        executeTaskAfterDelay("First Timer",8);
        executeTaskAfterDelay("Second Timer",5);
        executeTaskAfterDelay("Third Timer",2);

        executeTaskRepeatedly("First Timer",0,4);
        executeTaskRepeatedly("Second Timer",10,1);
        System.out.println("End of main method from thread " + Thread.currentThread().getName());
    }

    public static void executeTaskAfterDelay(String threadName, int seconds){
        TimerTask task = new TimerTask() {
            public void run() {
                System.out.println("Timer Task performed on: " + LocalDateTime.now() + " from thread " + Thread.currentThread().getName());
            }
        };
        Timer timer = new Timer(threadName);
        long delay = seconds * 1000L;
        timer.schedule(task, delay);
    }

    public static void executeTaskRepeatedly(String threadName, int initialDelay, int interval){
        TimerTask task = new TimerTask() {
            public void run() {
                System.out.println("Timer Task performed on: " + LocalDateTime.now() + " from thread " + Thread.currentThread().getName());
            }
        };
        Timer timer = new Timer(threadName);
        long delay = initialDelay * 1000L;
        long period = interval * 1000L;
        timer.scheduleAtFixedRate(task,delay,period);

    }

}
