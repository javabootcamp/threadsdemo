package org.bootcamp.java.threads.web.rest;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RESTController {

    @GetMapping("/")
    @ResponseBody
    public String helloWorld(){
        return "Hello from thread: " + Thread.currentThread().getName();
    }

}
